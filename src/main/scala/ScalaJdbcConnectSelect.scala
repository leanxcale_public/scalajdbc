import java.sql.DriverManager
import java.sql.Connection
import java.util
import com.leanxcale.kivi.query.TupleIterable
import com.leanxcale.kivi.session.impl.SessionImpl
import com.leanxcale.kivi.session.{Credentials, Session, SessionFactory, Settings}
import com.leanxcale.kivi.tuple.Tuple
import com.leanxcale.exception._
import com.leanxcale.kivi.query.expression.impl.KiviFieldExpression
import com.leanxcale.kivi.query.aggregation.Aggregations.max;
import com.leanxcale.kivi.query.aggregation.Aggregations.min;
import com.leanxcale.kivi.query.aggregation.Aggregations.avg;

import scala.collection.mutable.ListBuffer


object ScalaJdbcConnectSelect {

  def loadTableFromCsvJDBC(connection: Connection): Unit ={
    val statement = connection.prepareStatement("INSERT INTO JESUS.boston_housing values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
    val bufferedSource = scala.io.Source.fromFile("data/BostonHousing.csv")
    var columns = new ListBuffer[String]()
    for (line <- bufferedSource.getLines.drop(1)){
      val words = line.split(",").map(_.trim)

      statement.setDouble(1,words(0).toDouble)
      statement.setDouble(2,words(1).toDouble)
      statement.setDouble(3,words(2).toDouble)
      statement.setDouble(4,(words(3).substring(1, words(3).length()-1)).toDouble)
      statement.setDouble(5,words(4).toDouble)
      statement.setDouble(6,words(5).toDouble)
      statement.setDouble(7,words(6).toDouble)
      statement.setDouble(8,words(7).toDouble)
      statement.setDouble(9,words(8).toInt)
      statement.setDouble(10,words(9).toInt)
      statement.setDouble(11,words(10).toDouble)
      statement.setDouble(12,words(11).toDouble)
      statement.setDouble(13,words(12).toDouble)
      statement.setDouble(14,words(13).toDouble)

      statement.executeUpdate()
    }
  }

  def loadTableFromCsvKIVIAPI(session: Session, columns:Array[String]): Unit ={

    var database = session.database()
    var table = database.getTable("boston_housing")
    val bufferedSource = scala.io.Source.fromFile("data/BostonHousing.csv")
    for (line <- bufferedSource.getLines.drop(1)) {
      val words = line.split(",").map(_.trim)
      var tpl:Tuple = table.createTuple()
      tpl.putDouble(columns(0), words(0).toDouble)
      tpl.putDouble(columns(1), words(1).toDouble)
      tpl.putDouble(columns(2), words(2).toDouble)
      tpl.putDouble(columns(3), (words(3).substring(1, words(3).length()-1)).toDouble)
      tpl.putDouble(columns(4), words(4).toDouble)
      tpl.putDouble(columns(5), words(5).toDouble)
      tpl.putDouble(columns(6), words(6).toDouble)
      tpl.putDouble(columns(7), words(7).toDouble)
      tpl.putInteger(columns(8), words(8).toInt)
      tpl.putInteger(columns(9), words(9).toInt)
      tpl.putDouble(columns(10), words(10).toDouble)
      tpl.putDouble(columns(11), words(11).toDouble)
      tpl.putDouble(columns(12), words(12).toDouble)
      tpl.putDouble(columns(13), words(13).toDouble)

      table.insert(tpl)

    }
    session.commit()
  }

  def main(args: Array[String]) {

    val columns = Array("crim", "zn", "indus", "chas", "nox", "rm", "age", "dis", "rad", "tax",
    "ptratio", "b", "lstat", "medv")
    //JDBC Initialization
    val driver = "com.leanxcale.client.Driver"
    val url = "jdbc:leanxcale://localhost:1529/APP"
    val username = "JESUS"
    val password = "JESUS"
    var connection:Connection = null
    //Kivi Api initilization
    val pass: Array[Char] = Array('J', 'E', 'S', 'U', 'S')
    val credentials: Credentials = new Credentials().setUser("JESUS").setPass(pass).setDatabase("APP")
    val settings: Settings = new Settings().credentials(credentials)
    var session: Session = null
    var sessionImpl: SessionImpl = null
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
      session = SessionFactory.newSession("kivi:lxis://localhost:9876", settings)
//      loadTableFromCsvJDBC(connection)

      loadTableFromCsvKIVIAPI(session, columns)

      val statement = connection.createStatement()
      val selectAllRS = statement.executeQuery("SELECT * from JESUS.boston_housing")
      while (selectAllRS.next()) {
        /*println(selectAllRS.getDouble(columns(0)))
        println(selectAllRS.getDouble(columns(1)))
        println(selectAllRS.getDouble(columns(2)))
        println(selectAllRS.getDouble(columns(3)))
        println(selectAllRS.getDouble(columns(4)))
        println(selectAllRS.getDouble(columns(5)))
        println(selectAllRS.getDouble(columns(6)))
        println(selectAllRS.getDouble(columns(7)))
        println(selectAllRS.getDouble(columns(8)))
        println(selectAllRS.getDouble(columns(9)))
        println(selectAllRS.getDouble(columns(10)))
        println(selectAllRS.getDouble(columns(11)))
        println(selectAllRS.getDouble(columns(12)))
        println(selectAllRS.getDouble(columns(13)))*/
      }
      statement.close()
      //Data Exploration
      //Min, max, mean on MEDV
      val statementStats = connection.createStatement()
      val statsRS = statementStats.executeQuery("SELECT MIN(MEDV) AS MIN_PRICE, MAX(MEDV) AS " +
        "MAX_PRICE, AVG(MEDV) AS MEAN_PRICE FROM JESUS.boston_housing")
      while(statsRS.next()){
        val max = statsRS.getDouble("MAX_PRICE")
        val min = statsRS.getDouble("MIN_PRICE")
        val mean = statsRS.getDouble("MEAN_PRICE")
        println(s"Using JDBC: MEDV stats: MAX $max, MIN $min, MEAN $mean")
      }
      statementStats.close()
      //Get al MEDV values
      val statementMEDV = connection.createStatement()
      val MEDVRS = statementMEDV.executeQuery("SELECT MEDV FROM JESUS.boston_housing")
      val medvList = new Iterator[Double] {
        def hasNext = MEDVRS.next()
        def next() = MEDVRS.getDouble(1)
      }.toList
//      println(medvList)
      statementMEDV.close()
      connection.close()


      var database = session.database()
      var table = database.getTable("boston_housing")
      val  tplIterable:TupleIterable = table.find()
      val iter = tplIterable.iterator()
      while(iter.hasNext){
        val tpl:Tuple = iter.next()
//        println(tpl.toString)
      }

      var maxValue:Double=0
      var minValue:Double=0
      var meanValue:Double=0
      val aggList:java.util.List[String] = new util.ArrayList[String]()
      val tplItermax:TupleIterable = table.find().aggregate(aggList, max("max",new KiviFieldExpression("MEDV")))
      val iterMax = tplItermax.iterator()
      val tplItermin:TupleIterable = table.find().aggregate(aggList, min("min",new KiviFieldExpression("MEDV")))
      val iterMin = tplItermin.iterator()
      val tplItermean:TupleIterable = table.find().aggregate(aggList, avg("avg",new KiviFieldExpression("MEDV")))
      val iterMean = tplItermean.iterator()
      while(iterMax.hasNext){
        val tpl = iterMax.next()
        maxValue = tpl.getDouble("max")
      }
      while(iterMin.hasNext){
        val tpl = iterMin.next()
        minValue = tpl.getDouble("min")
      }
      while(iterMean.hasNext){
        val tpl = iterMean.next()
        meanValue = tpl.getDouble("avg")
      }
      println(s"Using Kivi Api: MEDV stats: MAX $maxValue, MIN $minValue, MEAN $meanValue")
      tplIterable.close()
      tplItermax.close()
      tplItermin.close()
      tplItermean.close()
      session.close()
    } catch {
      case e => e.printStackTrace
      case e:LeanxcaleException => e.printStackTrace
    }
  }

}