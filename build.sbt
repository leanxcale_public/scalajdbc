name := "scalaJDBC"

version := lxVersion

scalaVersion := "2.12.6"

val lxVersion = "1.6-RC.5"
//resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"
resolvers += "Leanxcale Nexus Repository" at "https://nexus.leanxcale.com/repository/maven-releases"

// Change this to another test framework if you prefer
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies += "com.leanxcale" % "kivi-api" % lxVersion classifier "jar-with-dependencies"
libraryDependencies += "com.leanxcale" % "qe-driver" % lxVersion classifier "jar-with-dependencies"


